provider "aws" {
  region = "us-east-1"
  access_key = "~~~"
  secret_key = "~~"
}
/*
resource "aws_instance" "LinuxServer" {
    ami = "ami-087c17d1fe0178315"
    instance_type = "t2.micro"
    tags = {
      name = "Jenkins"
      environment = "Dev"
    }
  
}

# SECTION 2 - ATTRIBUTES AND OUTPUTS


resource "aws_eip" "lb" {
  vpc      = true
}


output "eip" {
  value = aws_eip.lb.public_ip
}

resource "aws_s3_bucket" "mys3" {
  bucket = "kplabs-attribute-demo-0100"
}


output "mys3bucket" {
  value = aws_s3_bucket.mys3.arn
  
}

output "mys3bucket2" {
  value = aws_s3_bucket.mys3.id
  
}

# SECTION 2 - REFERNCING ATTRIBUTES OF EIP TO EC2

resource "aws_instance" "myec2" {
  ami = "ami-087c17d1fe0178315"
  instance_type = "t2.micro"
  
}

resource "aws_eip" "myeip" {
  vpc = true
  
}

resource "aws_eip_association" "eip-assoc" {
  instance_id = aws_instance.myec2.id
  allocation_id = aws_eip.myeip.id

  
}
output "myec22" {
  value = aws_instance.myec2.id  
}

output "my_eip" {
  value = aws_eip.myeip.public_ip
  
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_mytcp"
 
  ingress  {
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = ["${aws_eip.myeip.public_ip}/32"]
    }
  
}


resource "aws_security_group" "fatahSG" {
  name        = "fatahSG"
 
  ingress  {
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = [var.myIP]
    }

    ingress  {
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = [var.myIP]
    }

    ingress  {
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = [var.myIP]
    }
  
}


# SECTION 2 - MULTIPLE APPROACHES TO VARIABLE ASSIGNMENT

resource "aws_instance" "myRose" {
  ami = "ami-087c17d1fe0178315"
  instance_type = var.instance_type
  count = 3
}


# SECTION 2 - COUNT PARAMETER AND COUNT INDEX

resource "aws_iam_user" "lb" {
  name = var.elb_names[count.index]
  count = 3
  path = "/system/"
  
}


# SECTION 2 - CONDITIONAL EXPRESSION
# Reference variable in terraform.tfvars

variable "istest" {
  
}

resource "aws_instance" "dev" {
  ami = "ami-087c17d1fe0178315"
  instance_type = "t2.micro"
  count = var.istest == true ? 2 : 0  #if the value is true, create 2 instance else do not create
  
}

resource "aws_instance" "prod" {
  ami = "ami-087c17d1fe0178315"
  instance_type = "t2.small"
  count = var.istest == false ? 3 : 0  #if the value is false, create 3 instance else do not create
  
}


# SECTION 2 - LOCAL VALUES

locals {
  common_tags = {
    Owner = "DevOps Team"
    service = "backend"
  }
}
resource "aws_instance" "app-dev" {
   ami = "ami-082b5a644766e0e6f"
   instance_type = "t2.micro"
   tags = local.common_tags
}

resource "aws_instance" "db-dev" {
   ami = "ami-082b5a644766e0e6f"
   instance_type = "t2.small"
   tags = local.common_tags
}

resource "aws_ebs_volume" "db_ebs" {
  availability_zone = "us-west-2a"
  size              = 8
  tags = local.common_tags
}


# SECTION 2 - TERRAFORM FUNCTIONS

locals {
  time = formatdate("DD MMM YYYY hh:mm ZZZ", timestamp())
}

variable "region" {
  default = "ap-south-1"
}

variable "tags" {
  type = list
  default = ["firstec2","secondec2"]
}

variable "ami" {
  type = map
  default = {
    "us-east-1" = "ami-0323c3dd2da7fb37d"
    "us-west-2" = "ami-0d6621c01e8c2de2c"
    "ap-south-1" = "ami-0470e33cd681b2476"
  }
}

resource "aws_key_pair" "loginkey" {
  key_name   = "login-key"
  public_key = file("${path.module}/id_rsa.pub")
}

resource "aws_instance" "app-dev" {
   ami = lookup(var.ami,var.region)
   instance_type = "t2.micro"
   key_name = aws_key_pair.loginkey.key_name
   count = 2

   tags = {
     Name = element(var.tags,count.index)
   }
}


output "timestamp" {
  value = local.time
}

# SECTION 2 - DATA SOURCE

data "aws_ami" "app_ami" {
  most_recent = true
  owners = ["amazon"]


  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "instance-1" {
    ami = data.aws_ami.app_ami.id
   instance_type = "t2.micro"
}


# SECTION 2 - DEBUGGING IN TERRAFORM

#export TF_LOG_PATH=/tmp/crash.log  (RUN ON THE CMD LINE)
#export TF_LOG=TRACE (RUN ON THE CMD LINE)

resource "aws_instance" "db-dev" {
   ami = "ami-082b5a644766e0e6f"
   instance_type = "t2.small"

}



# SECTION 2 - TERRAFORM FORMAT

terraform fmt (use in cmd line to format/rearrange unstructured code file)


# SECTION 2 - VALIDATING TERRAFORM CONFIGURATION

resource "aws_instance" "myec2" {
  ami           = "ami-082b5a644766e0e6f"
  instance_type = "t2.micro"
  sky = "blue"
}


# SECTION 2 - LOAD ORDER AND SEMANTIC

# It is recommended to seperate each block into a separate file e.g provider, ec2, security_group, IAM_user
variable "iam_user" {
  default = "demouser"
}

resource "aws_instance" "myec2" {
   ami = "ami-082b5a644766e0e6f"
   instance_type = "t2.micro"
}

resource "aws_iam_user" "lb" {
  name = var.iam_user
  path = "/system/"
}


# SECTION 2 - DYNAMIC BLOCKS

resource "aws_security_group" "demo_sg" {
  name        = "sample-sg"

  ingress {
    from_port   = 8200
    to_port     = 8200
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8201
    to_port     = 8201
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8300
    to_port     = 8300
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9200
    to_port     = 9200
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9500
    to_port     = 9500
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

### dynamic-block.tf



variable "sg_ports" {
  type        = list(number)
  description = "list of ingress ports"
  default     = [8200, 8201,8300, 9200, 9500]
}

resource "aws_security_group" "dynamicsg" {
  name        = "dynamic-sg"
  description = "Ingress for Vault"

  dynamic "ingress" {
    for_each = var.sg_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "egress" {
    for_each = var.sg_ports
    content {
      from_port   = egress.value
      to_port     = egress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}



# SECTION 2 - TAINTING RESOUCES

resource "aws_instance" "myec2" {
  ami = "ami-082b5a644766e0e6f"
  instance_type = "t2.micro"
  
}



resource "aws_iam_user" "lb" {
  name = "iamuser.${count.index}"
  count = 3
  path = "/system/"
}

output "arns" {
  value = aws_iam_user.lb[1].arn
}


# SECTION 2 - TERRAFORM GRAPH

resource "aws_instance" "myec2" {
   ami = "ami-082b5a644766e0e6f"
   instance_type = "t2.micro"
}

resource "aws_eip" "lb" {
  instance = aws_instance.myec2.id
  vpc      = true
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${aws_eip.lb.private_ip}/32"]

  }
}



/*
### Commands Used:
```sh
terraform graph > graph.dot
yum install graphviz
cat graph.dot | dot -Tsvg > graph.svg
```




# SECTION 2 - SAVING TERRAFORM PLAN TO FILE

resource "aws_instance" "myec2" {
   ami = "ami-082b5a644766e0e6f"
   instance_type = "t2.micro"

}





# SECTION 2 - TERRAFORM OUTPUT

resource "aws_iam_user" "lb" {
  name = "iamuser.${count.index}"
  count = 3
  path = "/system/"
}

output "arns" {
  value = aws_iam_user.lb[*].arn
}

output "arns" {
  value = aws_iam_user.lb[*].id
}


# SECTION 3 - REMOTE-EXEC PROVISIONER

data "aws_ami" "app_ami" {
  most_recent = true
  owners = ["amazon"]


  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "fatah-ec2" {
   ami = data.aws_ami.app_ami.id
   instance_type = "t2.micro"
   key_name = "terraform-main"
  

   provisioner "remote-exec" {
     inline = [
       "sudo amazon-linux-extras install -y nginx1.12",
       "sudo systemctl start nginx"
     ]

   connection {
     type = "ssh"
     user = "ec2-user"
     private_key = file("./terraform-main.pem")
     host = self.public_ip
   }
   }
}

output "myec2-ip" {
  value = aws_instance.fatah-ec2.public_ip
  
}


# SECTION 3 - LOCAL-EXEC PROVISIONER
data "aws_ami" "app_ami" {
  most_recent = true
  owners = ["amazon"]


  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "fatah-ec2" {
   ami = data.aws_ami.app_ami.id
   instance_type = "t2.micro"

}

 provisioner "local-exec" {
    command = "echo ${aws_instance.fatah-ec2.private_ip} >> private_ips.txt"
  }



# SECTION 3 - PROVISIONER TYPES

data "aws_ami" "app_ami" {
  most_recent = true
  owners = ["amazon"]


  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"

  ingress {
    description = "SSH into VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "Outbound Allowed"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "myec2" {
   ami = data.aws_ami.app_ami.id
   instance_type = "t2.micro"
   key_name = "terraform-main"
   vpc_security_group_ids  = [aws_security_group.allow_ssh.id]

   provisioner "remote-exec" {
     inline = [
       "sudo yum -y install nano"
     ]
   }
   provisioner "remote-exec" {
       when    = destroy
       inline = [
         "sudo yum -y remove nano"
       ]
     }
   connection {
     type = "ssh"
     user = "ec2-user"
     private_key = file("./terraform-main.pem")
     host = self.public_ip
   }
}



# SECTION 3 - PROVISIONER FAILURE BEHAVIOR

data "aws_ami" "app_ami" {
  most_recent = true
  owners = ["amazon"]


  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"

  ingress {
    description = "SSH into VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "myec2" {
   ami = data.aws_ami.app_ami.id
   instance_type = "t2.micro"
   key_name = "terraform-main"
   vpc_security_group_ids  = [aws_security_group.allow_ssh.id]

   provisioner "remote-exec" {
     on_failure = continue
     inline = [
       "sudo yum -y install nano"
     ]
   }
   connection {
     type = "ssh"
     user = "ec2-user"
     private_key = file("./terraform-main.pem")
     host = self.public_ip
   }
}


 

# SECTION 4 - TERRAFORM REGISTRY
module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "single-instance"

  ami                    = "ami-082b5a644766e0e6f"
  instance_type          = "t2.micro"
  subnet_id              = "subnet-eddcdzz4"

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

 */

# SECTION 4 - TERRAFORM WORKSPACE

resource "aws_instance" "myec2" {
   ami = "ami-087c17d1fe0178315"
   instance_type = lookup(var.instance_type,terraform.workspace)
}

#We want to create different instance type for each terraform workspace
#deafult = t2.micro
#dev = t2.small
#prd = t2.large

variable "instance_type" {
  type = map

  default = {
    default = "t2.nano"
    dev     = "t2.micro"
    prd     = "t2.large"
  }
}